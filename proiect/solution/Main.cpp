#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"
#include "Camera.h"
#include "Model.h"

#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;

//functions
void renderFloor();
unsigned int CreateTexture(const std::string& strTexturePath);
unsigned int loadCubemap(vector<std::string> faces);

// camera
Camera camera(glm::vec3(-100.0f, 25.0f, 200.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;
float near_plane = 1.0f, far_plane = 48.0f;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}

	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Statie aero-spatiala", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	// configure global opengl state
   // -----------------------------
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	unsigned int floorTexture = CreateTexture("ColoredFloor.jpg");

	// configure global opengl state
   // -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------

	Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");
	Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");
	Shader skyboxShader("skybox.vs", "skybox.frag");

	// configure depth map FBO
	// -----------------------
	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// shader configuration
	// --------------------
	shadowMappingShader.use();
	shadowMappingShader.setInt("diffuseTexture", 0);
	shadowMappingShader.setInt("shadowMap", 1);


	Shader ourShader("lighting.vs", "lighting.fs");
	glm::vec3 lightPos(10.0f, 40.75f, 0.0f);
	ourShader.setInt("texture_diffuse1", 0);
	glm::vec3 lightColors(70.25f, 70.25f, 70.25f);


	// load models
	// -----------
	Model planeUh60("model/uh60/uh60.obj");
	Model building("model/tower/tower.blend");
	Model planeXFighter("model/Xfighter/Arc170.obj");
	Model moon("model/moon/moon.obj");
	Model tower1("model/4/Watch_Tower.obj");
	Model oldPlane("model/3/ww 1 for ele.obj");
	Model waterTank("model/water/Metal_Water_Tank/Water_Tank_BI.obj");
	Model armoredCar("model/armoredCar/Material/SDKFZ_251_Ausf_Armored_Car.blend");
	Model abraham("model/M1A2/Abrams_BF3.obj");
	Model wall("model/wall/wall.3DS");

	//skybox
	float skyboxVertices[] = {
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	// Setup skybox VAO
	unsigned int skyboxVAO, skyboxVBO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *)0);
	glBindVertexArray(0);

	// Cubemap (Skybox)
	vector<std::string> faces
	{
		"skybox\\skybox_right.tga",
		"skybox\\skybox_left.tga",
		"skybox\\skybox_top.tga",
		"skybox\\skybox_bottom.tga",
		"skybox\\skybox_front.tga",
		"skybox\\skybox_back.tga"
	};
	GLuint cubemapTexture = loadCubemap(faces);

	// draw in wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glEnable(GL_CULL_FACE);


	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.0f, 0.0f, 0.0f, 0.1f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// don't forget to enable shader before setting uniforms

		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;

		// render scene from light's point of view
		shadowMappingDepthShader.use();
		shadowMappingDepthShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);


		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 2.15f, 0.0f));
		model = glm::scale(model, glm::vec3(7.2f, 7.0f, 7.0f));
		shadowMappingDepthShader.setMat4("model", model);
		renderFloor();

		glm::mat4 modelUh = glm::mat4(1.0f);
		modelUh = glm::translate(modelUh, glm::vec3(20.0f, 7.75f, 15.0f));
		modelUh = glm::scale(modelUh, glm::vec3(1.4f, 1.4f, 1.4));
		modelUh = glm::rotate(modelUh, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingDepthShader.setMat4("model", modelUh);
		planeUh60.Draw(shadowMappingDepthShader);


		glm::mat4 modelBuilding = glm::mat4(1.0f);
		modelBuilding = glm::scale(modelBuilding, glm::vec3(1.0f, 1.0f, 1.0f));
		modelBuilding = glm::rotate(modelBuilding, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		modelBuilding = glm::translate(modelBuilding, glm::vec3(12.0f, -1.0f, -0.5f));
		shadowMappingDepthShader.setMat4("model", modelBuilding);
		building.Draw(shadowMappingDepthShader);

		glm::mat4 modelXFighter = glm::mat4(1.0f);
		modelXFighter = glm::scale(modelXFighter, glm::vec3(0.01f, 0.01f, 0.01f));
		//modelXFighter = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		modelXFighter = glm::translate(modelXFighter, glm::vec3(40.0f, -50.75f, -30.0f));
		shadowMappingDepthShader.setMat4("model", modelXFighter);
		planeXFighter.Draw(shadowMappingDepthShader);


		glm::mat4 modelWaterTank = glm::mat4(1.0f);
		modelWaterTank = glm::scale(modelWaterTank, glm::vec3(1.0f, 1.0f, 1.0f));
		modelWaterTank = glm::translate(modelWaterTank, glm::vec3(-17.5f, -1.5f, 5.0f));
		shadowMappingDepthShader.setMat4("model", modelWaterTank);
		waterTank.Draw(shadowMappingDepthShader);


		glm::mat4 modelTower1 = glm::mat4(1.0f);
		modelTower1 = glm::scale(modelTower1, glm::vec3(2.0f, 2.0f, 2.0f));
		modelTower1 = glm::translate(modelTower1, glm::vec3(-10.0f, -0.5f, -6.0f));
		shadowMappingDepthShader.setMat4("model", modelTower1);
		tower1.Draw(shadowMappingDepthShader);


		modelTower1 = glm::scale(modelTower1, glm::vec3(0.9f, 0.9f, 0.9f));
		modelTower1 = glm::translate(modelTower1, glm::vec3(30.0f, 0.0f, 0.0f));
		shadowMappingDepthShader.setMat4("model", modelTower1);
		tower1.Draw(shadowMappingDepthShader);

		modelTower1 = glm::scale(modelTower1, glm::vec3(0.9f, 0.9f, 0.9f));
		modelTower1 = glm::translate(modelTower1, glm::vec3(-33.0f, 0.0f, 25.0f));
		shadowMappingDepthShader.setMat4("model", modelTower1);
		tower1.Draw(shadowMappingDepthShader);

		modelTower1 = glm::scale(modelTower1, glm::vec3(1.0f, 1.0f, 1.0f));
		modelTower1 = glm::translate(modelTower1, glm::vec3(30.0f, 0.0f, 0.2f));
		shadowMappingDepthShader.setMat4("model", modelTower1);
		tower1.Draw(shadowMappingDepthShader);

		glm::mat4 modelArmoredCar = glm::mat4(1.0f);
		modelArmoredCar = glm::scale(modelArmoredCar, glm::vec3(1.0f, 1.0f, 1.0f));
		modelArmoredCar = glm::translate(modelArmoredCar, glm::vec3(28.0f, 1.85f, 26.0f));
		modelArmoredCar = glm::rotate(modelArmoredCar, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingDepthShader.setMat4("model", modelArmoredCar);
		armoredCar.Draw(shadowMappingDepthShader);

		modelArmoredCar = glm::scale(modelArmoredCar, glm::vec3(3.0f, 3.0f, 3.0f));
		modelArmoredCar = glm::translate(modelArmoredCar, glm::vec3(-50.0f, 1.85f, 0.0f));
		modelArmoredCar = glm::rotate(modelArmoredCar, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingDepthShader.setMat4("model", modelArmoredCar);
		armoredCar.Draw(shadowMappingDepthShader);

		//glm::mat4 modelPanzer = glm::mat4(1.0f);
		//modelPanzer = glm::scale(modelPanzer, glm::vec3(0.09f, 0.09f, 0.9f));
		//modelPanzer = glm::translate(modelPanzer, glm::vec3(20.0f, 1.85f, 40.0f));
		//modelPanzer = glm::rotate(modelPanzer, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		//shadowMappingDepthShader.setMat4("model", modelPanzer);
		//panzer.Draw(shadowMappingDepthShader);

		glm::mat4 modelAbraham = glm::mat4(1.0f);
		modelAbraham = glm::scale(modelAbraham, glm::vec3(3.0f, 3.0f, 3.0f));
		modelAbraham = glm::translate(modelAbraham, glm::vec3(6.0f, -0.5f, 12.0f));
		shadowMappingDepthShader.setMat4("model", modelAbraham);
		abraham.Draw(shadowMappingDepthShader);

		glm::mat4 modelWall = glm::mat4(1.0f);
		modelWall = glm::scale(modelWall, glm::vec3(0.5f, 0.3f, 0.6f));
		modelWall = glm::translate(modelWall, glm::vec3(-70.0f, 3.0f, -45.0f));
		shadowMappingDepthShader.setMat4("model", modelWall);
		wall.Draw(shadowMappingDepthShader);

		modelWall = glm::scale(modelWall, glm::vec3(0.5f, 0.3f, 0.95f));
		modelWall = glm::translate(modelWall, glm::vec3(3.0f, 160.0f, 6.0f));
		//modelWall = glm::rotate(modelWall, glm::radians(-90.0f), glm::vec3(0, 0, 1));
		shadowMappingDepthShader.setMat4("model", modelWall);
		wall.Draw(shadowMappingDepthShader);

		modelWall = glm::scale(modelWall, glm::vec3((1.1f, 1.1f, 1.1f)));
		modelWall = glm::translate(modelWall, glm::vec3(-4.6f, -140.0f, 0.5f));
		modelWall = glm::rotate(modelWall, glm::radians(-90.0f), glm::vec3(0, 1, 0));
		modelWall = glm::rotate(modelWall, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		modelWall = glm::rotate(modelWall, glm::radians(90.0f), glm::vec3(0, 0, 1));
		shadowMappingDepthShader.setMat4("model", modelWall);
		wall.Draw(shadowMappingDepthShader);

		glm::mat4 modelMoon = glm::mat4(1.0f);
		modelMoon = glm::scale(modelMoon, glm::vec3(0.1f, 0.1f, 0.1f));
		//modelMoon = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		modelMoon = glm::translate(modelMoon, glm::vec3(40.0f, 30.75f, -20.0f));
		shadowMappingDepthShader.setMat4("model", modelMoon);
		moon.Draw(shadowMappingDepthShader);


		//	renderScene(shadowMappingDepthShader);
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// reset viewport
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



		// 2. render scene as normal using the generated depth/shadow map 
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shadowMappingShader.use();
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 500.0f);
		glm::mat4 view = camera.GetViewMatrix();
		shadowMappingShader.setMat4("projection", projection);
		shadowMappingShader.setMat4("view", view);
		// set light uniforms
		shadowMappingShader.setVec3("viewPos", camera.Position);
		shadowMappingShader.setVec3("lightPos", lightPos);
		shadowMappingShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glDisable(GL_CULL_FACE);

		glm::mat4 model2 = glm::mat4(1.0f);
		model2 = glm::translate(model2, glm::vec3(0.0f, 2.15f, 0.0f));
		model2 = glm::scale(model2, glm::vec3(7.2f, 7.0f, 7.0f));
		shadowMappingShader.setMat4("model", model2);
		renderFloor();

		glm::mat4 modelUh2 = glm::mat4(1.0f);
		modelUh2 = glm::scale(modelUh2, glm::vec3(1.4f, 1.4f, 1.4));
		modelUh2 = glm::rotate(modelUh2, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		modelUh2 = glm::translate(modelUh2, glm::vec3(20.0f, 7.75f, 15.0f));
		shadowMappingShader.setMat4("model", modelUh2);
		planeUh60.Draw(shadowMappingShader);

		glm::mat4 modelBuilding2 = glm::mat4(1.0f);
		modelBuilding2 = glm::scale(modelBuilding2, glm::vec3(1.0f, 1.0f, 1.0f));
		modelBuilding2 = glm::translate(modelBuilding2, glm::vec3(12.0f, -1.0f, -0.5f));
		modelBuilding2 = glm::rotate(modelBuilding2, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingShader.setMat4("model", modelBuilding2);
		building.Draw(shadowMappingShader);

		glm::mat4 modelXFighter2 = glm::mat4(1.0f);
		modelXFighter2 = glm::scale(modelXFighter2, glm::vec3(0.01f, 0.01f, 0.01f));
		modelXFighter2 = glm::translate(modelXFighter2, glm::vec3(40.0f, -50.75f, -30.0f));
		shadowMappingShader.setMat4("model", modelXFighter2);
		planeXFighter.Draw(shadowMappingShader);


		glm::mat4 modelWaterTank2 = glm::mat4(1.0f);
		modelWaterTank2 = glm::scale(modelWaterTank2, glm::vec3(1.0f, 1.0f, 1.0f));
		modelWaterTank2 = glm::translate(modelWaterTank2, glm::vec3(-17.5f, -1.5f, 5.0f));
		shadowMappingShader.setMat4("model", modelWaterTank2);
		waterTank.Draw(shadowMappingShader);


		glm::mat4 modelTower12 = glm::mat4(1.0f);
		modelTower12 = glm::scale(modelTower12, glm::vec3(2.0f, 2.0f, 2.0f));
		modelTower12 = glm::translate(modelTower12, glm::vec3(-10.0f, -0.5f, -6.0f));
		shadowMappingShader.setMat4("model", modelTower12);
		tower1.Draw(shadowMappingShader);


		modelTower12 = glm::scale(modelTower12, glm::vec3(0.9f, 0.9f, 0.9f));
		modelTower12 = glm::translate(modelTower12, glm::vec3(30.0f, 0.0f, 0.0f));
		shadowMappingShader.setMat4("model", modelTower12);
		tower1.Draw(shadowMappingShader);

		modelTower12 = glm::scale(modelTower12, glm::vec3(0.9f, 0.9f, 0.9f));
		modelTower12 = glm::translate(modelTower12, glm::vec3(-33.0f, 0.0f, 25.0f));
		shadowMappingShader.setMat4("model", modelTower12);
		tower1.Draw(shadowMappingShader);


		modelTower12 = glm::scale(modelTower12, glm::vec3(1.0f, 1.0, 1.0f));
		modelTower12 = glm::translate(modelTower12, glm::vec3(30.0f, 0.0f, 0.2f));
		shadowMappingShader.setMat4("model", modelTower12);
		tower1.Draw(shadowMappingShader);

		glm::mat4 modelArmoredCar2 = glm::mat4(1.0f);
		modelArmoredCar2 = glm::scale(modelArmoredCar2, glm::vec3(2.0f, 2.0f, 2.0f));
		modelArmoredCar2 = glm::translate(modelArmoredCar2, glm::vec3(28.0f, 1.85f, 30.0f));
		modelArmoredCar2 = glm::rotate(modelArmoredCar2, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingShader.setMat4("model", modelArmoredCar2);
		armoredCar.Draw(shadowMappingShader);

		modelArmoredCar2 = glm::scale(modelArmoredCar2, glm::vec3(1.0f, 1.0f, 1.0f));
		modelArmoredCar2 = glm::translate(modelArmoredCar2, glm::vec3(-50.0f, 1.85f, 0.0f));
	//	modelArmoredCar2 = glm::rotate(modelArmoredCar2, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingShader.setMat4("model", modelArmoredCar2);
		armoredCar.Draw(shadowMappingShader);
	/*	glm::mat4 modelPanzer2 = glm::mat4(1.0f);
		modelPanzer2 = glm::scale(modelPanzer2, glm::vec3(0.09f, 0.09f, 0.09f));
		modelPanzer2 = glm::translate(modelPanzer2, glm::vec3(20.0f, 1.85f, 40.0f));
		modelPanzer2 = glm::rotate(modelPanzer2, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		shadowMappingShader.setMat4("model", modelPanzer2);
		panzer.Draw(shadowMappingShader);*/

		glm::mat4 modelAbraham2 = glm::mat4(1.0f);
		modelAbraham2 = glm::scale(modelAbraham2, glm::vec3(3.0f, 3.0f, 3.0f));
		modelAbraham2 = glm::translate(modelAbraham2, glm::vec3(6.0f, -0.5f, 12.0f));
		shadowMappingShader.setMat4("model", modelAbraham2);
		abraham.Draw(shadowMappingShader);

		glm::mat4 modelWall2 = glm::mat4(1.0f);
		modelWall2 = glm::scale(modelWall2, glm::vec3(0.5f, 0.3f, 0.6f));
		modelWall2 = glm::translate(modelWall2, glm::vec3(-70.0f, 3.0f, -45.0f));
		modelWall2 = glm::rotate(modelWall2, glm::radians(-90.0f), glm::vec3(0, 0, 1));
		shadowMappingShader.setMat4("model", modelWall2);
		wall.Draw(shadowMappingShader);

		modelWall2 = glm::scale(modelWall2, glm::vec3(1.0f, 1.0f, 0.95f));
		modelWall2 = glm::translate(modelWall2, glm::vec3(3.0f, 160.0f, 6.0f));
		//modelWall2 = glm::rotate(modelWall2, glm::radians(-90.0f), glm::vec3(0, 0, 1));
		shadowMappingShader.setMat4("model", modelWall2);
		wall.Draw(shadowMappingShader);

		modelWall2 = glm::scale(modelWall2, glm::vec3((1.1f, 1.1f,1.1f)));
		modelWall2 = glm::translate(modelWall2, glm::vec3(-4.6f,-140.0f, 0.5f));
		modelWall2 = glm::rotate(modelWall2, glm::radians(-90.0f), glm::vec3(0, 1, 0));
		modelWall2 = glm::rotate(modelWall2, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		modelWall2 = glm::rotate(modelWall2, glm::radians(90.0f), glm::vec3(0, 0, 1));
		shadowMappingShader.setMat4("model", modelWall2);
		wall.Draw(shadowMappingShader);


		glm::mat4 modelMoon2 = glm::mat4(1.0f);
		modelMoon2 = glm::translate(modelMoon2, glm::vec3(5.0f, 25.75f, -15.0f));
		modelMoon2 = glm::scale(modelMoon2, glm::vec3(0.1f, 0.1f, 0.1f));
		shadowMappingShader.setMat4("model", modelMoon2);
		moon.Draw(shadowMappingShader);

		//skybox

		// Draw skybox as last
		glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
		skyboxShader.use();
		view = glm::mat4(glm::mat3(camera.GetViewMatrix()));	// Remove any translation component of the view matrix

		glUniformMatrix4fv(glGetUniformLocation(skyboxShader.ID, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(skyboxShader.ID, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

		// skybox cube
		glBindVertexArray(skyboxVAO);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // Set depth function back to default

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	
	glfwTerminate();
	system("pause");
	return 0;
}
// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);


	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		near_plane += 0.5f;
		cout << far_plane << " far \n";
		cout << near_plane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		near_plane -= 0.5f;
		cout << far_plane << " far \n";
		cout << near_plane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
	{
		far_plane += 0.5f;
		cout << far_plane << " far \n";
		cout << near_plane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
	{
		far_plane -= 0.5f;
		cout << far_plane << " far \n";
		cout << near_plane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		far_plane = 0.1f;
		near_plane = 0.1f;
	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

unsigned int planeVAO = 0;
void renderFloor()
{
	unsigned int planeVBO;

	if (planeVAO == 0) {
		// set up vertex data (and buffer(s)) and configure vertex attributes
		float planeVertices[] = {
			// positions            // normals         // texcoords
			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
			25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
		};
		// plane VAO
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true); 
	unsigned char *data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}

unsigned int loadCubemap(vector<std::string> faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}